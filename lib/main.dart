import 'package:fast_cached_network_image/fast_cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:zamyatin_ay/utils/theme.dart';

import 'locator_setup.dart';
import 'ui/main_screen.dart';

Future<void> main() async {

  // инициализация библиботеки работы с изображениями
  await FastCachedImageConfig.init(clearCacheAfter: const Duration(days: 7));

  setupSingletons();

  runApp(MaterialApp(
      home: MainScreen(),
      theme: lightTheme()));
}
