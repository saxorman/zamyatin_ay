import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jovial_svg/jovial_svg.dart';

class SvgAssetWidget extends StatelessWidget {
  const SvgAssetWidget({super.key, required this.path, this.width, this.height, this.color, this.fit});

  final String path;
  final double? width;
  final double? height;
  final Color? color;
  final BoxFit? fit;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: ScalableImage.fromSvgAsset(rootBundle, path, currentColor: color),
      builder: (BuildContext context, AsyncSnapshot<ScalableImage> snapshot) {
        if (snapshot.hasData) {
          ScalableImage si = snapshot.data!;
          if (color != null) {
            si = si.modifyTint(newTintMode: BlendMode.srcIn, newTintColor: color!);
          }
          double scale = 1;
          if (si.width != null && width != null) {
            scale = width! / si.width!;
          }
          return ScalableImageWidget(si: si, scale: scale, fit: fit ?? BoxFit.contain);
        }
        return ScalableImageWidget(si: ScalableImage.blank());
      },
    );
  }
}