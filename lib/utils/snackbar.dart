import 'package:flutter/material.dart';

ScaffoldFeatureController showSnackBar(String message, BuildContext context,
    {Duration showingDuration = const Duration(seconds: 2)}) {
  var snackBar = SnackBar(
      content: Text(message),
      behavior: SnackBarBehavior.fixed,
      duration: showingDuration);
  ScaffoldMessenger.of(context).hideCurrentSnackBar();
  return ScaffoldMessenger.of(context).showSnackBar(snackBar);
}