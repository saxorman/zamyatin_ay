import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

ThemeData lightTheme() {
  return ThemeData(
    useMaterial3: true,
    fontFamily: "Roboto",
    focusColor: const Color(0xFF0061A2),

    appBarTheme: const AppBarTheme(
      iconTheme: IconThemeData(
        color: Color(0xFF0094FF),
      ),
      // systemOverlayStyle: SystemUiOverlayStyle.light,
      backgroundColor: Color(0xFFFFFFFF),
      titleTextStyle: TextStyle(color: Color(0xFF1A1C1E), fontSize: 25, fontStyle: FontStyle.normal, fontWeight: FontWeight.w600, height: 1.33, overflow: TextOverflow.fade),
    ),

    colorScheme: const ColorScheme(
      primary: Color(0xFFFFFFFF),
      onPrimary: Color(0xFF000000),
      primaryContainer: Color(0xFFD1E4FF),
      onPrimaryContainer: Color(0xFF001D35),
      secondary: Color(0xFF535F70),
      onSecondary: Color(0xFFFFFFFF),
      secondaryContainer: Color(0xFFD6E4F7),
      onSecondaryContainer: Color(0xFF0F1C2B),
      onTertiary: Color(0xFFFFFFFF),
      onTertiaryContainer: Color(0xFF251432),
      error: Color(0xFFBA1A1A),
      onError: Color(0xFFFFFFFF),
      errorContainer: Color(0xFFFFDAD6),
      onErrorContainer: Color(0xFF410002),
      background: Color(0xFFFDFCFF),
      onBackground: Color(0xFF1A1C1E),
      surface: Color(0xFFFDFCFF),
      onSurface: Color(0xFF1A1C1E),
      surfaceVariant: Color(0xFFDFE2EB),
      onSurfaceVariant: Color(0xFF42474E),
      outline: Color(0xFF73777F),
      brightness: Brightness.light,
    ),

    dividerColor: const Color(0xFF969696),
    // applyElevationOverlayColor: true,

    textTheme: const TextTheme(
      displayLarge: TextStyle(color: Color(0xFF1A1C1E), fontSize: 57, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, height: 1.12, letterSpacing: -0.25),
      displayMedium: TextStyle(color: Color(0xFF1A1C1E), fontSize: 45, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, height: 1.16),
      displaySmall: TextStyle(color: Color(0xFF1A1C1E), fontSize: 36, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, height: 1.22),

      headlineLarge: TextStyle(color: Color(0xFF1A1C1E), fontSize: 32, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, height: 1.25),
      headlineMedium: TextStyle(color: Color(0xFF1A1C1E), fontSize: 28, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, height: 1.29),
      headlineSmall: TextStyle(color: Color(0xFF1A1C1E), fontSize: 24, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, height: 1.33),

      titleLarge: TextStyle(color: Color(0xFF1A1C1E), fontSize: 20, fontStyle: FontStyle.normal, fontWeight: FontWeight.w600, height: 1.27),
      titleMedium: TextStyle(color: Color(0xFF1A1C1E), fontSize: 16, fontStyle: FontStyle.normal, fontWeight: FontWeight.w500, height: 1.5, letterSpacing: 0.15),
      titleSmall: TextStyle(color: Color(0xFF1A1C1E), fontSize: 14, fontStyle: FontStyle.normal, fontWeight: FontWeight.w500, height: 1.43, letterSpacing: 0.1),

      labelLarge: TextStyle(color: Color(0xFF1A1C1E), fontSize: 14, fontStyle: FontStyle.normal, fontWeight: FontWeight.w500, height: 1.43, letterSpacing: 0.1),
      labelMedium: TextStyle(color: Color(0xFF1A1C1E), fontSize: 12, fontStyle: FontStyle.normal, fontWeight: FontWeight.w500, height: 1.33, letterSpacing: 0.5),
      labelSmall: TextStyle(color: Color(0xFF1A1C1E), fontSize: 11, fontStyle: FontStyle.normal, fontWeight: FontWeight.w500, height: 1.45, letterSpacing: 0.5),

      bodyLarge: TextStyle(color: Color(0xFF1A1C1E), fontSize: 16, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, height: 1.5, letterSpacing: 0.5),
      bodyMedium: TextStyle(color: Color(0x99000000), fontSize: 14, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, height: 1.43, letterSpacing: 0.25),
      bodySmall: TextStyle(color: Color(0xFF1A1C1E), fontSize: 12, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, height: 1.33, letterSpacing: 0.4),
    ),

    listTileTheme: const ListTileThemeData(
      iconColor: Color(0xFF0094FF),
      titleTextStyle: TextStyle(color: Color(0xFF1A1C1E), fontSize: 16, fontStyle: FontStyle.normal, fontWeight: FontWeight.w500, height: 1.5, letterSpacing: 0.15),
      subtitleTextStyle: TextStyle(color: Color(0x991A1C1E), fontSize: 16, fontStyle: FontStyle.normal, fontWeight: FontWeight.w500, height: 1.5, letterSpacing: 0.15),
    ),

    iconTheme: const IconThemeData(color: Color(0xFF0094FF)),

    inputDecorationTheme: const InputDecorationTheme(
      labelStyle: TextStyle(color: Color(0xFF979797), fontSize: 16, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, letterSpacing: 0.5),
      hintStyle: TextStyle(color: Color(0xFF979797), fontSize: 16, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, letterSpacing: 0.5),
    ),

    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        textStyle: MaterialStateProperty.all(const TextStyle(fontSize: 14, color: Color(0xFFFFFFFF), fontStyle: FontStyle.normal, fontWeight: FontWeight.w500, height: 1.43)),
        backgroundColor: MaterialStateProperty.all(const Color(0xFF0094FF)),
        foregroundColor: MaterialStateProperty.all(const Color(0xFFFFFFFF)),
        shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0),)),
        fixedSize: MaterialStateProperty.all(const Size(160, 45)),
        elevation: MaterialStateProperty.resolveWith((states) {
          return 0;
        }),
      ),
    ),

    cardColor: const Color(0xFFFFFFFF),

    snackBarTheme: SnackBarThemeData(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      backgroundColor: const Color(0xFF1A1C1E), // onSurface
      contentTextStyle: const TextStyle(color: Color(0xFFFFFFFF), fontSize: 14, fontStyle: FontStyle.normal, fontWeight: FontWeight.w400, height: 1.43, letterSpacing: 0.25),
      elevation: 8,
    )
  );
}