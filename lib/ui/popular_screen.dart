import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:zamyatin_ay/api/types/film_minimized_entity.dart';
import 'package:zamyatin_ay/ui/film_list_item.dart';
import 'package:zamyatin_ay/ui/network_error_widget.dart';
import 'package:zamyatin_ay/view_model/popular_screen_view_model.dart';

class PopularScreen extends StatefulWidget {
  const PopularScreen({Key? key, this.searchStream}) : super(key: key);

  final Stream<List<FilmMinimizedEntity>?>? searchStream;

  @override
  State<PopularScreen> createState() => _PopularScreenState();
}

class _PopularScreenState extends State<PopularScreen> {
  final _viewModel = PopularScreenViewModel();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _viewModel,
      child: StreamBuilder<List<FilmMinimizedEntity>?>(
        stream: widget.searchStream ?? _viewModel.filmsStream,
        builder: (BuildContext context,
            AsyncSnapshot<List<FilmMinimizedEntity>?> snapshot) {
          if (snapshot.connectionState != ConnectionState.active ||
              (!snapshot.hasError && !snapshot.hasData)) {
            return const Center(child: CircularProgressIndicator());
          }

          if (snapshot.hasError) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40.0),
              child: LoadErrorWidget(
                  retryCallback: () {
                    _viewModel.loadFilms();
                  },
                  isNetworkError: snapshot.error is IOException),
            );
          }

          final films = snapshot.data ?? List.unmodifiable([]);

          if (films.isEmpty) {
            return const Center(child: Text("Не найдено"));
          }

          return ListView.separated(
              itemCount: films.length,
              itemBuilder: (context, index) {
                return FilmListItem(
                    key: ValueKey(films[index].filmId),
                    film: films[index],
                    isFavorite: _viewModel.filmIsFavorite(films[index].filmId),
                    longPressCallback: () async {
                      await _viewModel.processFavorite(films[index]);
                      setState(() {});
                    });
              },
              separatorBuilder: (context, index) {
                return const SizedBox(height: 8);
              });
        },
      ),
    );
  }
}
