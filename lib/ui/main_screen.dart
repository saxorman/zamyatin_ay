import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:zamyatin_ay/ui/favorite_films_screen.dart';
import 'package:zamyatin_ay/view_model/main_screen_view_model.dart';

import 'popular_screen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final PageController controller =
      PageController(initialPage: 0, keepPage: true);
  
  final MainScreenViewModel _viewModel = MainScreenViewModel();

  final ValueNotifier<String> _appBarTitle = ValueNotifier("Популярные");
  final ValueNotifier<bool> _searchMode = ValueNotifier(false);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => MainScreenViewModel(),
      child: Scaffold(
        appBar: SearchAppBar(
            viewModel: _viewModel,
            appBarTitle: _appBarTitle,
            searchMode: _searchMode),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
                child: PageView(
                    physics: const NeverScrollableScrollPhysics(),
                    controller: controller,
                    children: [
                  ValueListenableBuilder(
                    valueListenable: _searchMode,
                    builder: (context, searchMode, child) {
                      return PopularScreen(
                          key: const PageStorageKey("popular"),
                          searchStream:
                              searchMode ? _viewModel.filmsStream : null);
                    },
                  ),
                  FavoriteFilmsScreen(
                      key: const PageStorageKey("favorite"),
                      filter: _viewModel.searchText)
                ])),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                children: [
                  ElevatedButton(
                    style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
                      backgroundColor: const MaterialStatePropertyAll(Color(0xFFDEEFFF)),
                      foregroundColor: const MaterialStatePropertyAll(Color(0xFF0094FF)),
                    ),
                      onPressed: () => switchToPage(0, "Популярные"),
                      child: const Text("Популярные")),
                  ElevatedButton(
                      onPressed: () => switchToPage(1, "Избранное"),
                      child: const Text("Избранное"))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void switchToPage(int page, String title) {
    controller.animateToPage(page,
        duration: const Duration(milliseconds: 500),
        curve: Curves.fastOutSlowIn);
    _appBarTitle.value = title;
  }
}

class SearchAppBar extends StatefulWidget implements PreferredSizeWidget {
  const SearchAppBar(
      {Key? key, required this.viewModel, required this.appBarTitle, required this.searchMode})
      : super(key: key);

  final MainScreenViewModel viewModel;
  final ValueNotifier<String> appBarTitle;
  final ValueNotifier<bool> searchMode;

  @override
  State<SearchAppBar> createState() => _SearchAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(56);
}

class _SearchAppBarState extends State<SearchAppBar> {

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: widget.searchMode,
      builder: (BuildContext context, bool value, Widget? child) {
        if (value == true) {
          return SearchStateAppBar(
            searchMode: widget.searchMode,
            viewModel: widget.viewModel,
          );
        } else {
          return PreferredSize(
            preferredSize: const Size.fromHeight(56),
            child: AppBar(
              title: ValueListenableBuilder(
                  valueListenable: widget.appBarTitle,
                  builder: (context, value, child) {
                    return Text(value);
                  }),
              actions: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 18.0),
                  child: InkResponse(
                    radius: 32,
                    child: const Icon(Icons.search),
                    onTap: () async {
                      widget.searchMode.value = !widget.searchMode.value;
                    },
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}

class SearchStateAppBar extends StatefulWidget {
  const SearchStateAppBar({Key? key, required this.searchMode, required this.viewModel}) : super(key: key);
  
  final ValueNotifier<bool> searchMode;
  final MainScreenViewModel viewModel;

  @override
  State<SearchStateAppBar> createState() => _SearchStateAppBarState();
}

class _SearchStateAppBarState extends State<SearchStateAppBar> {
  final TextEditingController _searchTextController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _searchTextController.addListener(changeText);
  }

  void changeText() {
    widget.viewModel.setSearchText(_searchTextController.text);
  }

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: const Size.fromHeight(56),
      child: AppBar(
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.blue),
          onPressed: () {
            setState(() {
              _searchTextController.text = "";
              widget.searchMode.value = false;
            });
          },
        ),
        title: SizedBox(
          height: 40,
          child: TextField(
            controller: _searchTextController,
            maxLines: 1,
            style: Theme
                .of(context)
                .textTheme
                .bodyLarge,

            decoration: const InputDecoration(
              border: InputBorder.none,
              filled: false,
              hintText: "Поиск",
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _searchTextController.removeListener(changeText);
    _searchTextController.dispose();
  }
}
