import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:zamyatin_ay/api/types/film_minimized_entity.dart';
import 'package:zamyatin_ay/ui/film_list_item.dart';
import 'package:zamyatin_ay/view_model/favorite_films_screen_view_model.dart';

class FavoriteFilmsScreen extends StatefulWidget {
  const FavoriteFilmsScreen({Key? key, required this.filter}) : super(key: key);

  final Stream<String> filter;

  @override
  State<FavoriteFilmsScreen> createState() => _FavoriteFilmsScreenState();
}

class _FavoriteFilmsScreenState extends State<FavoriteFilmsScreen> with AutomaticKeepAliveClientMixin<FavoriteFilmsScreen> {
  late final _viewModel = FavoriteFilmsScreenViewModel(widget.filter);

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ChangeNotifierProvider.value(
      value: _viewModel,
      child: StreamBuilder<List<FilmMinimizedEntity>>(
        stream: _viewModel.filmsStream,
        builder: (BuildContext context, AsyncSnapshot<List<FilmMinimizedEntity>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }

          final films = snapshot.data ?? [];

          if (!snapshot.hasData || snapshot.hasError) {
            return const Center(
              child: Text("Ошибка получения списка"),
            );
          }

          if (films.isEmpty) {
            return const Center(child: Text("Не найдено"));
          }

          return ListView.separated(
              itemCount: films.length,
              itemBuilder: (context, index) {
                return FilmListItem(
                  key: ValueKey(films[index].filmId),
                    film: films[index],
                    isFavorite: _viewModel.filmIsFavorite(films[index].filmId),
                    longPressCallback: () async {
                      await _viewModel.processFavorite(films[index]);
                    });
              },
              separatorBuilder: (context, index) {
                return const SizedBox(height: 8);
              });
        },
      ),
    );
  }
}
