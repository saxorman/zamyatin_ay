import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:zamyatin_ay/api/types/film_minimized_entity.dart';
import 'package:zamyatin_ay/ui/film_list_item.dart';
import 'package:zamyatin_ay/utils/snackbar.dart';
import 'package:zamyatin_ay/view_model/detail_screen_view_model.dart';

class DetailFilmScreen extends StatefulWidget {
  const DetailFilmScreen({super.key, required this.film});

  final FilmMinimizedEntity film;

  @override
  State<DetailFilmScreen> createState() => _DetailFilmScreenState();
}

class _DetailFilmScreenState extends State<DetailFilmScreen> {
  final DetailScreenViewModel _viewModel = DetailScreenViewModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
          headerSliverBuilder: (context, innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                leading: IconButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.white12)),
                    onPressed: () => Navigator.of(context).pop(),
                    icon: const Icon(Icons.arrow_back)),
                foregroundColor: Colors.blue,
                expandedHeight: 600,
                floating: true,
                pinned: false,
                flexibleSpace: FlexibleSpaceBar(
                  stretchModes: const [StretchMode.zoomBackground],
                  background: FilmPoster(
                    key: ValueKey(widget.film.filmId),
                    url: widget.film.posterUrl,
                    fit: BoxFit.fill,
                  ),
                ),
              )
            ];
          },
          body: ChangeNotifierProvider.value(
            value: _viewModel,
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: ListView(
                  children: [
                    Align(
                        alignment: Alignment.center,
                        child: Text(widget.film.nameRu ?? "",
                            style: Theme.of(context).textTheme.titleLarge)),
                    FutureBuilder(
                      future: _viewModel.getFullFilmInfo(widget.film.filmId),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState != ConnectionState.done) {
                          return const SizedBox(
                              height: 50,
                              width: 50,
                              child:
                                  Center(child: CircularProgressIndicator()));
                        }

                        if (snapshot.hasError) {
                          Future.delayed(
                              const Duration(milliseconds: 200),
                              () => showSnackBar(
                                  "Ошибка получения описания", context));
                        }

                        final fullFilmInfo = snapshot.data;

                        return fullFilmInfo?.description != null ? Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: Text(fullFilmInfo!.description!, style: Theme.of(context).textTheme.bodyMedium),
                        ) : const SizedBox.shrink();
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                          "Жанры: ${widget.film.genres?.map((e) => e.genre)}"),
                    ),
                    Text(
                        "Страны: ${widget.film.countries?.map((e) => e.country)}"),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text("Год: ${widget.film.year}"),
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _viewModel.dispose();
  }
}
