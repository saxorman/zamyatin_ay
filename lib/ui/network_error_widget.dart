import 'package:flutter/material.dart';
import 'package:zamyatin_ay/utils/svg_asset_widget.dart';

class LoadErrorWidget extends StatelessWidget {
  const LoadErrorWidget(
      {super.key, required this.retryCallback, required this.isNetworkError});

  final VoidCallback retryCallback;
  final bool isNetworkError;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        if (isNetworkError)
          const SvgAssetWidget(
              path: "resources/images/network_error.svg", width: 100),
        Text(
            textAlign: TextAlign.center,
            isNetworkError
                ? "Произошла ошибка при загрузке данных, проверьте подключение к сети"
                : "Ошибка загрузки",
        style: Theme.of(context).textTheme.bodyMedium?.copyWith(color: const Color(0xFF0094FF))),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: SizedBox(
            width: 124,
            child: ElevatedButton(
                onPressed: () => retryCallback.call(), child: Text("Повторить")),
          ),
        )
      ],
    );
  }
}
