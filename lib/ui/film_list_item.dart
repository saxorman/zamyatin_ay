import 'package:fast_cached_network_image/fast_cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:collection/collection.dart';
import 'package:zamyatin_ay/api/types/film_minimized_entity.dart';
import 'package:zamyatin_ay/ui/detail_film_screen.dart';

class FilmListItem extends StatelessWidget {
  const FilmListItem(
      {Key? key, required this.film, required this.longPressCallback, required this.isFavorite})
      : super(key: key);

  final FilmMinimizedEntity film;
  final VoidCallback longPressCallback;
  final bool isFavorite;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: SizedBox(
        child: Card(
          elevation: 12.0,
          child: ListTile(
            contentPadding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12))),
            leading: FilmPoster(url: film.posterUrlPreview, fit: BoxFit.fitHeight,),
            title: Text(film.nameRu ?? "Нет названия",
                softWrap: true, overflow: TextOverflow.ellipsis),
            subtitle: Text(_getSubtitle()),
            onLongPress: () => longPressCallback.call(),
            trailing: isFavorite
                ? const Icon(
                    Icons.star,
                    size: 18,
                  )
                : null,
            onTap: () => showDetailScreen(context),
          ),
        ),
      ),
    );
  }

  void showDetailScreen(BuildContext context) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => DetailFilmScreen(film: film)));
  }

  String _getSubtitle() {
    final genre =
        film.genres?.firstWhereOrNull((g) => g.genre.isNotEmpty)?.genre;
    return "${genre ?? ''} (${film.year})";
  }
}

class FilmPoster extends StatelessWidget {
  const FilmPoster({super.key, this.url, this.width, this.height, this.fit});

  final String? url;
  final double? width;
  final double? height;
  final BoxFit? fit;

  @override
  Widget build(BuildContext context) {
    return FastCachedImage(
      key: ValueKey(url),
      width: width,
      height: height,
      url: url!,
      fit: fit,
      errorBuilder: (context, error, stackTrace) {
        return const Icon(Icons.broken_image_outlined, size: 32);
      },
    );
  }
}
