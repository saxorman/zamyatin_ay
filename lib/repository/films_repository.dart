import 'package:zamyatin_ay/api/kinopoisk_api.dart';
import 'package:zamyatin_ay/api/types/film_entity.dart';
import 'package:zamyatin_ay/api/types/film_minimized_entity.dart';
import 'package:zamyatin_ay/database/source/films_database_source.dart';

class FilmsRepository {
  FilmsRepository(this._kinopoiskApi, this._filmsDatabaseSource);

  final KinopoiskApi _kinopoiskApi;
  final FilmsDatabaseSource _filmsDatabaseSource;

  Future<List<FilmMinimizedEntity>> getTopFilms() async {
    final response = await _kinopoiskApi.api.getTopFilms();

    return response.films;
  }

  Future<List<FilmMinimizedEntity>> getFavoritesFilms() async {
    final films = await _filmsDatabaseSource.getAllFilmsMinimized();

    return films;
  }

  Future<FilmEntity?> getFilmInfo(int id) async {
    FilmEntity? film;
    try {
      film = await _kinopoiskApi.api.getFilmById(id);
    } catch (e) {
      print(e);
    }

    if (film == null) {
      try {
        film = await _filmsDatabaseSource.getFullFilmInfoById(id);
      } catch (e) {
        print(e);
      }
    }

    return film;
  }

  Future<bool> removeFavoriteFromDatabase(int id) async {
    try {
      await _filmsDatabaseSource.deleteFullFillInfo(id);
      return _filmsDatabaseSource.deleteMinimized(id);
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> addFilmMinimizedFavoriteToDatabase(FilmMinimizedEntity film) async {
    try {
      _cacheFilmFullInfo(film.filmId);
      return _filmsDatabaseSource.insertFilmMinimized(film);
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> addFavoriteFilmToDatabase(FilmEntity film) async {
    try {
      _cacheFilmFullInfo(film.kinopoiskId);
      return _filmsDatabaseSource.insertFilmFullInfo(film);
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<void> _cacheFilmFullInfo(int id) async {
    try {
      final film = await _kinopoiskApi.api.getFilmById(id);
      await _filmsDatabaseSource.insertFilmFullInfo(film);
    } catch (e) {
      print("Error cache film full info: $e");
    }
  }

  Future<List<FilmMinimizedEntity>> searchFilms(String keyword) async {
    List<FilmMinimizedEntity> films = [];

    try {
      final response = await _kinopoiskApi.api.searchFilms(keyword);
      //bad idea
      films = response.items
          .map((e) => FilmMinimizedEntity(
              e.kinopoiskId,
              e.ratingKinopoisk?.toString(),
              e.nameRu,
              e.nameEn,
              "",
              e.posterUrl,
              e.posterUrlPreview,
              "",
              null,
              e.year?.toString(),
              e.description,
              e.countries,
              e.genres))
          .toList();
    } catch (e) {
      print("Error cache film full info: $e");
    }

    return films;
  }
}
