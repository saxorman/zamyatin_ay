import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:zamyatin_ay/api/types/film_minimized_entity.dart';
import 'package:zamyatin_ay/models/films_model.dart';

class PopularScreenViewModel extends ChangeNotifier {

  PopularScreenViewModel() {
    if (_filmsModel.filmsList.isEmpty) {
      loadFilms();
    }
  }

  final FilmsModel _filmsModel = GetIt.I.get<FilmsModel>();

  Stream<List<FilmMinimizedEntity>?> get filmsStream => _filmsModel.filmsStream;
  List<FilmMinimizedEntity> get filmsList => _filmsModel.filmsList;

  Future<void> loadFilms() async {
    try {
      await _filmsModel.getFilms();
    } catch (e) {
      debugPrint("Error loading films: $e");
    }
  }

  Future<void> processFavorite(FilmMinimizedEntity film) =>
      _filmsModel.processFavorite(film);

  bool filmIsFavorite(int filmId) => _filmsModel.filmIsFavorite(filmId);
}
