import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:zamyatin_ay/api/types/film_entity.dart';
import 'package:zamyatin_ay/models/films_model.dart';

class DetailScreenViewModel extends ChangeNotifier {
  final FilmsModel _filmsModel = GetIt.I.get<FilmsModel>();

  Future<FilmEntity?> getFullFilmInfo(int filmId) =>
      _filmsModel.getFullFilmInfo(filmId);
}
