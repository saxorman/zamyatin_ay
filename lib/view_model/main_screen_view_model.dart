import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';
import 'package:zamyatin_ay/api/types/film_minimized_entity.dart';
import 'package:zamyatin_ay/repository/films_repository.dart';

class MainScreenViewModel extends ChangeNotifier {
  final FilmsRepository _repository = GetIt.I.get<FilmsRepository>();

  final int _debounceTimeMs = 500;

  final BehaviorSubject<String> _searchText = BehaviorSubject.seeded("");

  Stream<String> get searchText =>
      _searchText.stream.debounceTime(Duration(milliseconds: _debounceTimeMs));

  final BehaviorSubject<List<FilmMinimizedEntity>?> _filmsSubject =
      BehaviorSubject.seeded([]);

  List<FilmMinimizedEntity> get filmsList =>
      _filmsSubject.value ?? List.unmodifiable([]);

  Stream<List<FilmMinimizedEntity>?> get filmsStream => _filmsSubject.stream;

  Timer? _requestTimer;

  void setSearchText(String text) {
    _searchText.add(text);
    _requestTimer?.cancel();
    if (text.isNotEmpty) {
      _requestTimer = Timer(Duration(milliseconds: _debounceTimeMs), () {
        _searchFilms(_searchText.value);
      });
    } else {
      _filmsSubject.add([]);
    }
  }

  Future<void> _searchFilms(String keyword) async {
    final films = await _repository.searchFilms(keyword);
    _filmsSubject.add(films);
  }
}
