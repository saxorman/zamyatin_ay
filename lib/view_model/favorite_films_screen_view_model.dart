import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';
import 'package:zamyatin_ay/api/types/film_minimized_entity.dart';
import 'package:zamyatin_ay/models/films_model.dart';

class FavoriteFilmsScreenViewModel extends ChangeNotifier {
  FavoriteFilmsScreenViewModel(Stream<String> filterStream) {
    filmsStream = Rx.combineLatest2(
        _filmsModel.favoriteFilmsStream,
        filterStream,
        (list, filter) => list.where((element) {
              final str = filter.toLowerCase();
              return (element.nameRu?.toLowerCase().contains(str) ?? false) ||
                  (element.nameEn?.toLowerCase().contains(str) ?? false);
            }).toList());
  }

  final FilmsModel _filmsModel = GetIt.I.get<FilmsModel>();

  late final Stream<List<FilmMinimizedEntity>> filmsStream;

  List<FilmMinimizedEntity> get filmsList => _filmsModel.favoriteFilmsList;

  // Future<void> loadFilms() async {
  //   if (_filmsIsLoading.value) {
  //     return;
  //   }
  //
  //   _filmsIsLoading.add(true);
  //
  //   try {
  //     await _filmsModel.getFilms();
  //   } catch (e) {
  //     debugPrint("Error loading films: $e");
  //   } finally {
  //     _filmsIsLoading.add(false);
  //   }
  // }

  Future<void> processFavorite(FilmMinimizedEntity film) =>
      _filmsModel.processFavorite(film);

  bool filmIsFavorite(int filmId) => _filmsModel.filmIsFavorite(filmId);
}
