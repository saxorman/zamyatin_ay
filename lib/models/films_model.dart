import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';
import 'package:zamyatin_ay/api/types/film_entity.dart';
import 'package:zamyatin_ay/api/types/film_minimized_entity.dart';
import 'package:zamyatin_ay/repository/films_repository.dart';

class FilmsModel extends Disposable {
  FilmsModel(this._repository) {
    getFavoriteFilms();
  }

  final FilmsRepository _repository;

  //*** popular
  final BehaviorSubject<List<FilmMinimizedEntity>?> _filmsSubject =
      BehaviorSubject.seeded([]);

  List<FilmMinimizedEntity> get filmsList => _filmsSubject.value ?? List.unmodifiable([]);

  Stream<List<FilmMinimizedEntity>?> get filmsStream => _filmsSubject.stream;
  //***

  //*** favorite
  final BehaviorSubject<Map<int, FilmMinimizedEntity>> _favoriteFilmsSubject =
      BehaviorSubject.seeded({});

  List<FilmMinimizedEntity> get favoriteFilmsList =>
      _favoriteFilmsSubject.value.values.toList();

  Stream<List<FilmMinimizedEntity>> get favoriteFilmsStream =>
      _favoriteFilmsSubject.stream.map((event) => event.values.toList());
  //***

  final Map<int, FilmEntity> filmEntities = {};

  Future<List<FilmMinimizedEntity>> getFilms() async {
    _filmsSubject.add(null);
    List<FilmMinimizedEntity> films = [];
    try {
      films = await _repository.getTopFilms();
      _filmsSubject.add(films);
      return films;
    } on DioException catch(e, _) {
      Exception error;
      if (e.error != null && e.error is Exception) {
        error = e.error! as Exception;
      } else {
        error = Exception("Error loading films");
      }
      _filmsSubject.addError(error);
      return films;
    } catch (e, stackTrace) {
      _filmsSubject.addError(e);
      debugPrint("Error load top films: $e");
      print(stackTrace);
      return films;
    }
  }

  Future<FilmEntity?> getFullFilmInfo(int filmId) async {
    FilmEntity? targetFilm = filmEntities[filmId];

    if (targetFilm == null) {
      targetFilm = await _repository.getFilmInfo(filmId);
      if (targetFilm != null) {
        filmEntities[filmId] = targetFilm;
      }
    }

    return targetFilm;
  }

  Future<List<FilmMinimizedEntity>> getFavoriteFilms() async {
    try {
      final films = await _repository.getFavoritesFilms();
      Map<int, FilmMinimizedEntity> map = {};
      for (var film in films) {
        map[film.filmId] = film;
      }

      _favoriteFilmsSubject.add(map);

      return films;
    } catch (e, stackTrace) {
      debugPrint("Error load favorite films: $e");
      print(stackTrace);
      return [];
    }
  }

  Future<void> processFavorite(FilmMinimizedEntity film) async {
    final favorites = _favoriteFilmsSubject.value;
    if (filmIsFavorite(film.filmId)) {
      await _repository.removeFavoriteFromDatabase(film.filmId);
      favorites.remove(film.filmId);
      _favoriteFilmsSubject.add(favorites);
    } else {
      await _repository.addFilmMinimizedFavoriteToDatabase(film);
      favorites[film.filmId] = film;
      _favoriteFilmsSubject.add(favorites);
    }
  }

  bool filmIsFavorite(int filmId) {
    return _favoriteFilmsSubject.value.containsKey(filmId);
  }

  @override
  FutureOr onDispose() {}
}
