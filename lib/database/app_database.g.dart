// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// ignore_for_file: type=lint
class $FilmMinimizedTableTable extends FilmMinimizedTable
    with TableInfo<$FilmMinimizedTableTable, FilmDbMinimized> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FilmMinimizedTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _filmIdMeta = const VerificationMeta('filmId');
  @override
  late final GeneratedColumn<int> filmId = GeneratedColumn<int>(
      'film_id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _ratingMeta = const VerificationMeta('rating');
  @override
  late final GeneratedColumn<String> rating = GeneratedColumn<String>(
      'rating', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _nameRuMeta = const VerificationMeta('nameRu');
  @override
  late final GeneratedColumn<String> nameRu = GeneratedColumn<String>(
      'name_ru', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _nameEnMeta = const VerificationMeta('nameEn');
  @override
  late final GeneratedColumn<String> nameEn = GeneratedColumn<String>(
      'name_en', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _typeMeta = const VerificationMeta('type');
  @override
  late final GeneratedColumn<String> type = GeneratedColumn<String>(
      'type', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _posterUrlMeta =
      const VerificationMeta('posterUrl');
  @override
  late final GeneratedColumn<String> posterUrl = GeneratedColumn<String>(
      'poster_url', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _posterUrlPreviewMeta =
      const VerificationMeta('posterUrlPreview');
  @override
  late final GeneratedColumn<String> posterUrlPreview = GeneratedColumn<String>(
      'poster_url_preview', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _filmLengthMeta =
      const VerificationMeta('filmLength');
  @override
  late final GeneratedColumn<String> filmLength = GeneratedColumn<String>(
      'film_length', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _ratingVoteCountMeta =
      const VerificationMeta('ratingVoteCount');
  @override
  late final GeneratedColumn<int> ratingVoteCount = GeneratedColumn<int>(
      'rating_vote_count', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _yearMeta = const VerificationMeta('year');
  @override
  late final GeneratedColumn<String> year = GeneratedColumn<String>(
      'year', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String> description = GeneratedColumn<String>(
      'description', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _countriesMeta =
      const VerificationMeta('countries');
  @override
  late final GeneratedColumn<String> countries = GeneratedColumn<String>(
      'countries', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _genresMeta = const VerificationMeta('genres');
  @override
  late final GeneratedColumn<String> genres = GeneratedColumn<String>(
      'genres', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [
        filmId,
        rating,
        nameRu,
        nameEn,
        type,
        posterUrl,
        posterUrlPreview,
        filmLength,
        ratingVoteCount,
        year,
        description,
        countries,
        genres
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'film_minimized';
  @override
  VerificationContext validateIntegrity(Insertable<FilmDbMinimized> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('film_id')) {
      context.handle(_filmIdMeta,
          filmId.isAcceptableOrUnknown(data['film_id']!, _filmIdMeta));
    }
    if (data.containsKey('rating')) {
      context.handle(_ratingMeta,
          rating.isAcceptableOrUnknown(data['rating']!, _ratingMeta));
    }
    if (data.containsKey('name_ru')) {
      context.handle(_nameRuMeta,
          nameRu.isAcceptableOrUnknown(data['name_ru']!, _nameRuMeta));
    }
    if (data.containsKey('name_en')) {
      context.handle(_nameEnMeta,
          nameEn.isAcceptableOrUnknown(data['name_en']!, _nameEnMeta));
    }
    if (data.containsKey('type')) {
      context.handle(
          _typeMeta, type.isAcceptableOrUnknown(data['type']!, _typeMeta));
    }
    if (data.containsKey('poster_url')) {
      context.handle(_posterUrlMeta,
          posterUrl.isAcceptableOrUnknown(data['poster_url']!, _posterUrlMeta));
    }
    if (data.containsKey('poster_url_preview')) {
      context.handle(
          _posterUrlPreviewMeta,
          posterUrlPreview.isAcceptableOrUnknown(
              data['poster_url_preview']!, _posterUrlPreviewMeta));
    }
    if (data.containsKey('film_length')) {
      context.handle(
          _filmLengthMeta,
          filmLength.isAcceptableOrUnknown(
              data['film_length']!, _filmLengthMeta));
    }
    if (data.containsKey('rating_vote_count')) {
      context.handle(
          _ratingVoteCountMeta,
          ratingVoteCount.isAcceptableOrUnknown(
              data['rating_vote_count']!, _ratingVoteCountMeta));
    }
    if (data.containsKey('year')) {
      context.handle(
          _yearMeta, year.isAcceptableOrUnknown(data['year']!, _yearMeta));
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('countries')) {
      context.handle(_countriesMeta,
          countries.isAcceptableOrUnknown(data['countries']!, _countriesMeta));
    }
    if (data.containsKey('genres')) {
      context.handle(_genresMeta,
          genres.isAcceptableOrUnknown(data['genres']!, _genresMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {filmId};
  @override
  FilmDbMinimized map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FilmDbMinimized(
      filmId: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}film_id'])!,
      rating: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}rating']),
      nameRu: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name_ru']),
      nameEn: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name_en']),
      type: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}type']),
      posterUrl: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}poster_url']),
      posterUrlPreview: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}poster_url_preview']),
      filmLength: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}film_length']),
      ratingVoteCount: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}rating_vote_count']),
      year: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}year']),
      description: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}description']),
      countries: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}countries']),
      genres: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}genres']),
    );
  }

  @override
  $FilmMinimizedTableTable createAlias(String alias) {
    return $FilmMinimizedTableTable(attachedDatabase, alias);
  }
}

class FilmDbMinimized extends DataClass implements Insertable<FilmDbMinimized> {
  final int filmId;
  final String? rating;
  final String? nameRu;
  final String? nameEn;
  final String? type;
  final String? posterUrl;
  final String? posterUrlPreview;
  final String? filmLength;
  final int? ratingVoteCount;
  final String? year;
  final String? description;
  final String? countries;
  final String? genres;
  const FilmDbMinimized(
      {required this.filmId,
      this.rating,
      this.nameRu,
      this.nameEn,
      this.type,
      this.posterUrl,
      this.posterUrlPreview,
      this.filmLength,
      this.ratingVoteCount,
      this.year,
      this.description,
      this.countries,
      this.genres});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['film_id'] = Variable<int>(filmId);
    if (!nullToAbsent || rating != null) {
      map['rating'] = Variable<String>(rating);
    }
    if (!nullToAbsent || nameRu != null) {
      map['name_ru'] = Variable<String>(nameRu);
    }
    if (!nullToAbsent || nameEn != null) {
      map['name_en'] = Variable<String>(nameEn);
    }
    if (!nullToAbsent || type != null) {
      map['type'] = Variable<String>(type);
    }
    if (!nullToAbsent || posterUrl != null) {
      map['poster_url'] = Variable<String>(posterUrl);
    }
    if (!nullToAbsent || posterUrlPreview != null) {
      map['poster_url_preview'] = Variable<String>(posterUrlPreview);
    }
    if (!nullToAbsent || filmLength != null) {
      map['film_length'] = Variable<String>(filmLength);
    }
    if (!nullToAbsent || ratingVoteCount != null) {
      map['rating_vote_count'] = Variable<int>(ratingVoteCount);
    }
    if (!nullToAbsent || year != null) {
      map['year'] = Variable<String>(year);
    }
    if (!nullToAbsent || description != null) {
      map['description'] = Variable<String>(description);
    }
    if (!nullToAbsent || countries != null) {
      map['countries'] = Variable<String>(countries);
    }
    if (!nullToAbsent || genres != null) {
      map['genres'] = Variable<String>(genres);
    }
    return map;
  }

  FilmMinimizedTableCompanion toCompanion(bool nullToAbsent) {
    return FilmMinimizedTableCompanion(
      filmId: Value(filmId),
      rating:
          rating == null && nullToAbsent ? const Value.absent() : Value(rating),
      nameRu:
          nameRu == null && nullToAbsent ? const Value.absent() : Value(nameRu),
      nameEn:
          nameEn == null && nullToAbsent ? const Value.absent() : Value(nameEn),
      type: type == null && nullToAbsent ? const Value.absent() : Value(type),
      posterUrl: posterUrl == null && nullToAbsent
          ? const Value.absent()
          : Value(posterUrl),
      posterUrlPreview: posterUrlPreview == null && nullToAbsent
          ? const Value.absent()
          : Value(posterUrlPreview),
      filmLength: filmLength == null && nullToAbsent
          ? const Value.absent()
          : Value(filmLength),
      ratingVoteCount: ratingVoteCount == null && nullToAbsent
          ? const Value.absent()
          : Value(ratingVoteCount),
      year: year == null && nullToAbsent ? const Value.absent() : Value(year),
      description: description == null && nullToAbsent
          ? const Value.absent()
          : Value(description),
      countries: countries == null && nullToAbsent
          ? const Value.absent()
          : Value(countries),
      genres:
          genres == null && nullToAbsent ? const Value.absent() : Value(genres),
    );
  }

  factory FilmDbMinimized.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FilmDbMinimized(
      filmId: serializer.fromJson<int>(json['filmId']),
      rating: serializer.fromJson<String?>(json['rating']),
      nameRu: serializer.fromJson<String?>(json['nameRu']),
      nameEn: serializer.fromJson<String?>(json['nameEn']),
      type: serializer.fromJson<String?>(json['type']),
      posterUrl: serializer.fromJson<String?>(json['posterUrl']),
      posterUrlPreview: serializer.fromJson<String?>(json['posterUrlPreview']),
      filmLength: serializer.fromJson<String?>(json['filmLength']),
      ratingVoteCount: serializer.fromJson<int?>(json['ratingVoteCount']),
      year: serializer.fromJson<String?>(json['year']),
      description: serializer.fromJson<String?>(json['description']),
      countries: serializer.fromJson<String?>(json['countries']),
      genres: serializer.fromJson<String?>(json['genres']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'filmId': serializer.toJson<int>(filmId),
      'rating': serializer.toJson<String?>(rating),
      'nameRu': serializer.toJson<String?>(nameRu),
      'nameEn': serializer.toJson<String?>(nameEn),
      'type': serializer.toJson<String?>(type),
      'posterUrl': serializer.toJson<String?>(posterUrl),
      'posterUrlPreview': serializer.toJson<String?>(posterUrlPreview),
      'filmLength': serializer.toJson<String?>(filmLength),
      'ratingVoteCount': serializer.toJson<int?>(ratingVoteCount),
      'year': serializer.toJson<String?>(year),
      'description': serializer.toJson<String?>(description),
      'countries': serializer.toJson<String?>(countries),
      'genres': serializer.toJson<String?>(genres),
    };
  }

  FilmDbMinimized copyWith(
          {int? filmId,
          Value<String?> rating = const Value.absent(),
          Value<String?> nameRu = const Value.absent(),
          Value<String?> nameEn = const Value.absent(),
          Value<String?> type = const Value.absent(),
          Value<String?> posterUrl = const Value.absent(),
          Value<String?> posterUrlPreview = const Value.absent(),
          Value<String?> filmLength = const Value.absent(),
          Value<int?> ratingVoteCount = const Value.absent(),
          Value<String?> year = const Value.absent(),
          Value<String?> description = const Value.absent(),
          Value<String?> countries = const Value.absent(),
          Value<String?> genres = const Value.absent()}) =>
      FilmDbMinimized(
        filmId: filmId ?? this.filmId,
        rating: rating.present ? rating.value : this.rating,
        nameRu: nameRu.present ? nameRu.value : this.nameRu,
        nameEn: nameEn.present ? nameEn.value : this.nameEn,
        type: type.present ? type.value : this.type,
        posterUrl: posterUrl.present ? posterUrl.value : this.posterUrl,
        posterUrlPreview: posterUrlPreview.present
            ? posterUrlPreview.value
            : this.posterUrlPreview,
        filmLength: filmLength.present ? filmLength.value : this.filmLength,
        ratingVoteCount: ratingVoteCount.present
            ? ratingVoteCount.value
            : this.ratingVoteCount,
        year: year.present ? year.value : this.year,
        description: description.present ? description.value : this.description,
        countries: countries.present ? countries.value : this.countries,
        genres: genres.present ? genres.value : this.genres,
      );
  @override
  String toString() {
    return (StringBuffer('FilmDbMinimized(')
          ..write('filmId: $filmId, ')
          ..write('rating: $rating, ')
          ..write('nameRu: $nameRu, ')
          ..write('nameEn: $nameEn, ')
          ..write('type: $type, ')
          ..write('posterUrl: $posterUrl, ')
          ..write('posterUrlPreview: $posterUrlPreview, ')
          ..write('filmLength: $filmLength, ')
          ..write('ratingVoteCount: $ratingVoteCount, ')
          ..write('year: $year, ')
          ..write('description: $description, ')
          ..write('countries: $countries, ')
          ..write('genres: $genres')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      filmId,
      rating,
      nameRu,
      nameEn,
      type,
      posterUrl,
      posterUrlPreview,
      filmLength,
      ratingVoteCount,
      year,
      description,
      countries,
      genres);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FilmDbMinimized &&
          other.filmId == this.filmId &&
          other.rating == this.rating &&
          other.nameRu == this.nameRu &&
          other.nameEn == this.nameEn &&
          other.type == this.type &&
          other.posterUrl == this.posterUrl &&
          other.posterUrlPreview == this.posterUrlPreview &&
          other.filmLength == this.filmLength &&
          other.ratingVoteCount == this.ratingVoteCount &&
          other.year == this.year &&
          other.description == this.description &&
          other.countries == this.countries &&
          other.genres == this.genres);
}

class FilmMinimizedTableCompanion extends UpdateCompanion<FilmDbMinimized> {
  final Value<int> filmId;
  final Value<String?> rating;
  final Value<String?> nameRu;
  final Value<String?> nameEn;
  final Value<String?> type;
  final Value<String?> posterUrl;
  final Value<String?> posterUrlPreview;
  final Value<String?> filmLength;
  final Value<int?> ratingVoteCount;
  final Value<String?> year;
  final Value<String?> description;
  final Value<String?> countries;
  final Value<String?> genres;
  const FilmMinimizedTableCompanion({
    this.filmId = const Value.absent(),
    this.rating = const Value.absent(),
    this.nameRu = const Value.absent(),
    this.nameEn = const Value.absent(),
    this.type = const Value.absent(),
    this.posterUrl = const Value.absent(),
    this.posterUrlPreview = const Value.absent(),
    this.filmLength = const Value.absent(),
    this.ratingVoteCount = const Value.absent(),
    this.year = const Value.absent(),
    this.description = const Value.absent(),
    this.countries = const Value.absent(),
    this.genres = const Value.absent(),
  });
  FilmMinimizedTableCompanion.insert({
    this.filmId = const Value.absent(),
    this.rating = const Value.absent(),
    this.nameRu = const Value.absent(),
    this.nameEn = const Value.absent(),
    this.type = const Value.absent(),
    this.posterUrl = const Value.absent(),
    this.posterUrlPreview = const Value.absent(),
    this.filmLength = const Value.absent(),
    this.ratingVoteCount = const Value.absent(),
    this.year = const Value.absent(),
    this.description = const Value.absent(),
    this.countries = const Value.absent(),
    this.genres = const Value.absent(),
  });
  static Insertable<FilmDbMinimized> custom({
    Expression<int>? filmId,
    Expression<String>? rating,
    Expression<String>? nameRu,
    Expression<String>? nameEn,
    Expression<String>? type,
    Expression<String>? posterUrl,
    Expression<String>? posterUrlPreview,
    Expression<String>? filmLength,
    Expression<int>? ratingVoteCount,
    Expression<String>? year,
    Expression<String>? description,
    Expression<String>? countries,
    Expression<String>? genres,
  }) {
    return RawValuesInsertable({
      if (filmId != null) 'film_id': filmId,
      if (rating != null) 'rating': rating,
      if (nameRu != null) 'name_ru': nameRu,
      if (nameEn != null) 'name_en': nameEn,
      if (type != null) 'type': type,
      if (posterUrl != null) 'poster_url': posterUrl,
      if (posterUrlPreview != null) 'poster_url_preview': posterUrlPreview,
      if (filmLength != null) 'film_length': filmLength,
      if (ratingVoteCount != null) 'rating_vote_count': ratingVoteCount,
      if (year != null) 'year': year,
      if (description != null) 'description': description,
      if (countries != null) 'countries': countries,
      if (genres != null) 'genres': genres,
    });
  }

  FilmMinimizedTableCompanion copyWith(
      {Value<int>? filmId,
      Value<String?>? rating,
      Value<String?>? nameRu,
      Value<String?>? nameEn,
      Value<String?>? type,
      Value<String?>? posterUrl,
      Value<String?>? posterUrlPreview,
      Value<String?>? filmLength,
      Value<int?>? ratingVoteCount,
      Value<String?>? year,
      Value<String?>? description,
      Value<String?>? countries,
      Value<String?>? genres}) {
    return FilmMinimizedTableCompanion(
      filmId: filmId ?? this.filmId,
      rating: rating ?? this.rating,
      nameRu: nameRu ?? this.nameRu,
      nameEn: nameEn ?? this.nameEn,
      type: type ?? this.type,
      posterUrl: posterUrl ?? this.posterUrl,
      posterUrlPreview: posterUrlPreview ?? this.posterUrlPreview,
      filmLength: filmLength ?? this.filmLength,
      ratingVoteCount: ratingVoteCount ?? this.ratingVoteCount,
      year: year ?? this.year,
      description: description ?? this.description,
      countries: countries ?? this.countries,
      genres: genres ?? this.genres,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (filmId.present) {
      map['film_id'] = Variable<int>(filmId.value);
    }
    if (rating.present) {
      map['rating'] = Variable<String>(rating.value);
    }
    if (nameRu.present) {
      map['name_ru'] = Variable<String>(nameRu.value);
    }
    if (nameEn.present) {
      map['name_en'] = Variable<String>(nameEn.value);
    }
    if (type.present) {
      map['type'] = Variable<String>(type.value);
    }
    if (posterUrl.present) {
      map['poster_url'] = Variable<String>(posterUrl.value);
    }
    if (posterUrlPreview.present) {
      map['poster_url_preview'] = Variable<String>(posterUrlPreview.value);
    }
    if (filmLength.present) {
      map['film_length'] = Variable<String>(filmLength.value);
    }
    if (ratingVoteCount.present) {
      map['rating_vote_count'] = Variable<int>(ratingVoteCount.value);
    }
    if (year.present) {
      map['year'] = Variable<String>(year.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (countries.present) {
      map['countries'] = Variable<String>(countries.value);
    }
    if (genres.present) {
      map['genres'] = Variable<String>(genres.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FilmMinimizedTableCompanion(')
          ..write('filmId: $filmId, ')
          ..write('rating: $rating, ')
          ..write('nameRu: $nameRu, ')
          ..write('nameEn: $nameEn, ')
          ..write('type: $type, ')
          ..write('posterUrl: $posterUrl, ')
          ..write('posterUrlPreview: $posterUrlPreview, ')
          ..write('filmLength: $filmLength, ')
          ..write('ratingVoteCount: $ratingVoteCount, ')
          ..write('year: $year, ')
          ..write('description: $description, ')
          ..write('countries: $countries, ')
          ..write('genres: $genres')
          ..write(')'))
        .toString();
  }
}

class $FullFilmInfoTableTable extends FullFilmInfoTable
    with TableInfo<$FullFilmInfoTableTable, FilmDbEntity> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FullFilmInfoTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _kinopoiskIdMeta =
      const VerificationMeta('kinopoiskId');
  @override
  late final GeneratedColumn<int> kinopoiskId = GeneratedColumn<int>(
      'kinopoisk_id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _imdbIdMeta = const VerificationMeta('imdbId');
  @override
  late final GeneratedColumn<String> imdbId = GeneratedColumn<String>(
      'imdb_id', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _nameRuMeta = const VerificationMeta('nameRu');
  @override
  late final GeneratedColumn<String> nameRu = GeneratedColumn<String>(
      'name_ru', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _nameEnMeta = const VerificationMeta('nameEn');
  @override
  late final GeneratedColumn<String> nameEn = GeneratedColumn<String>(
      'name_en', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _nameOriginalMeta =
      const VerificationMeta('nameOriginal');
  @override
  late final GeneratedColumn<String> nameOriginal = GeneratedColumn<String>(
      'name_original', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _posterUrlMeta =
      const VerificationMeta('posterUrl');
  @override
  late final GeneratedColumn<String> posterUrl = GeneratedColumn<String>(
      'poster_url', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _posterUrlPreviewMeta =
      const VerificationMeta('posterUrlPreview');
  @override
  late final GeneratedColumn<String> posterUrlPreview = GeneratedColumn<String>(
      'poster_url_preview', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _ratingKinopoiskMeta =
      const VerificationMeta('ratingKinopoisk');
  @override
  late final GeneratedColumn<double> ratingKinopoisk = GeneratedColumn<double>(
      'rating_kinopoisk', aliasedName, true,
      type: DriftSqlType.double, requiredDuringInsert: false);
  static const VerificationMeta _ratingImdbMeta =
      const VerificationMeta('ratingImdb');
  @override
  late final GeneratedColumn<double> ratingImdb = GeneratedColumn<double>(
      'rating_imdb', aliasedName, true,
      type: DriftSqlType.double, requiredDuringInsert: false);
  static const VerificationMeta _yearMeta = const VerificationMeta('year');
  @override
  late final GeneratedColumn<int> year = GeneratedColumn<int>(
      'year', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String> description = GeneratedColumn<String>(
      'description', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _shortDescriptionMeta =
      const VerificationMeta('shortDescription');
  @override
  late final GeneratedColumn<String> shortDescription = GeneratedColumn<String>(
      'short_description', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _countriesMeta =
      const VerificationMeta('countries');
  @override
  late final GeneratedColumn<String> countries = GeneratedColumn<String>(
      'countries', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _genresMeta = const VerificationMeta('genres');
  @override
  late final GeneratedColumn<String> genres = GeneratedColumn<String>(
      'genres', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [
        kinopoiskId,
        imdbId,
        nameRu,
        nameEn,
        nameOriginal,
        posterUrl,
        posterUrlPreview,
        ratingKinopoisk,
        ratingImdb,
        year,
        description,
        shortDescription,
        countries,
        genres
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'films_table';
  @override
  VerificationContext validateIntegrity(Insertable<FilmDbEntity> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('kinopoisk_id')) {
      context.handle(
          _kinopoiskIdMeta,
          kinopoiskId.isAcceptableOrUnknown(
              data['kinopoisk_id']!, _kinopoiskIdMeta));
    }
    if (data.containsKey('imdb_id')) {
      context.handle(_imdbIdMeta,
          imdbId.isAcceptableOrUnknown(data['imdb_id']!, _imdbIdMeta));
    }
    if (data.containsKey('name_ru')) {
      context.handle(_nameRuMeta,
          nameRu.isAcceptableOrUnknown(data['name_ru']!, _nameRuMeta));
    }
    if (data.containsKey('name_en')) {
      context.handle(_nameEnMeta,
          nameEn.isAcceptableOrUnknown(data['name_en']!, _nameEnMeta));
    }
    if (data.containsKey('name_original')) {
      context.handle(
          _nameOriginalMeta,
          nameOriginal.isAcceptableOrUnknown(
              data['name_original']!, _nameOriginalMeta));
    }
    if (data.containsKey('poster_url')) {
      context.handle(_posterUrlMeta,
          posterUrl.isAcceptableOrUnknown(data['poster_url']!, _posterUrlMeta));
    }
    if (data.containsKey('poster_url_preview')) {
      context.handle(
          _posterUrlPreviewMeta,
          posterUrlPreview.isAcceptableOrUnknown(
              data['poster_url_preview']!, _posterUrlPreviewMeta));
    }
    if (data.containsKey('rating_kinopoisk')) {
      context.handle(
          _ratingKinopoiskMeta,
          ratingKinopoisk.isAcceptableOrUnknown(
              data['rating_kinopoisk']!, _ratingKinopoiskMeta));
    }
    if (data.containsKey('rating_imdb')) {
      context.handle(
          _ratingImdbMeta,
          ratingImdb.isAcceptableOrUnknown(
              data['rating_imdb']!, _ratingImdbMeta));
    }
    if (data.containsKey('year')) {
      context.handle(
          _yearMeta, year.isAcceptableOrUnknown(data['year']!, _yearMeta));
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('short_description')) {
      context.handle(
          _shortDescriptionMeta,
          shortDescription.isAcceptableOrUnknown(
              data['short_description']!, _shortDescriptionMeta));
    }
    if (data.containsKey('countries')) {
      context.handle(_countriesMeta,
          countries.isAcceptableOrUnknown(data['countries']!, _countriesMeta));
    }
    if (data.containsKey('genres')) {
      context.handle(_genresMeta,
          genres.isAcceptableOrUnknown(data['genres']!, _genresMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {kinopoiskId};
  @override
  FilmDbEntity map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FilmDbEntity(
      kinopoiskId: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}kinopoisk_id'])!,
      imdbId: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}imdb_id']),
      nameRu: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name_ru']),
      nameEn: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name_en']),
      nameOriginal: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name_original']),
      posterUrl: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}poster_url']),
      posterUrlPreview: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}poster_url_preview']),
      ratingKinopoisk: attachedDatabase.typeMapping.read(
          DriftSqlType.double, data['${effectivePrefix}rating_kinopoisk']),
      ratingImdb: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}rating_imdb']),
      year: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}year']),
      description: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}description']),
      shortDescription: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}short_description']),
      countries: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}countries']),
      genres: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}genres']),
    );
  }

  @override
  $FullFilmInfoTableTable createAlias(String alias) {
    return $FullFilmInfoTableTable(attachedDatabase, alias);
  }
}

class FilmDbEntity extends DataClass implements Insertable<FilmDbEntity> {
  final int kinopoiskId;
  final String? imdbId;
  final String? nameRu;
  final String? nameEn;
  final String? nameOriginal;
  final String? posterUrl;
  final String? posterUrlPreview;
  final double? ratingKinopoisk;
  final double? ratingImdb;
  final int? year;
  final String? description;
  final String? shortDescription;
  final String? countries;
  final String? genres;
  const FilmDbEntity(
      {required this.kinopoiskId,
      this.imdbId,
      this.nameRu,
      this.nameEn,
      this.nameOriginal,
      this.posterUrl,
      this.posterUrlPreview,
      this.ratingKinopoisk,
      this.ratingImdb,
      this.year,
      this.description,
      this.shortDescription,
      this.countries,
      this.genres});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['kinopoisk_id'] = Variable<int>(kinopoiskId);
    if (!nullToAbsent || imdbId != null) {
      map['imdb_id'] = Variable<String>(imdbId);
    }
    if (!nullToAbsent || nameRu != null) {
      map['name_ru'] = Variable<String>(nameRu);
    }
    if (!nullToAbsent || nameEn != null) {
      map['name_en'] = Variable<String>(nameEn);
    }
    if (!nullToAbsent || nameOriginal != null) {
      map['name_original'] = Variable<String>(nameOriginal);
    }
    if (!nullToAbsent || posterUrl != null) {
      map['poster_url'] = Variable<String>(posterUrl);
    }
    if (!nullToAbsent || posterUrlPreview != null) {
      map['poster_url_preview'] = Variable<String>(posterUrlPreview);
    }
    if (!nullToAbsent || ratingKinopoisk != null) {
      map['rating_kinopoisk'] = Variable<double>(ratingKinopoisk);
    }
    if (!nullToAbsent || ratingImdb != null) {
      map['rating_imdb'] = Variable<double>(ratingImdb);
    }
    if (!nullToAbsent || year != null) {
      map['year'] = Variable<int>(year);
    }
    if (!nullToAbsent || description != null) {
      map['description'] = Variable<String>(description);
    }
    if (!nullToAbsent || shortDescription != null) {
      map['short_description'] = Variable<String>(shortDescription);
    }
    if (!nullToAbsent || countries != null) {
      map['countries'] = Variable<String>(countries);
    }
    if (!nullToAbsent || genres != null) {
      map['genres'] = Variable<String>(genres);
    }
    return map;
  }

  FullFilmInfoTableCompanion toCompanion(bool nullToAbsent) {
    return FullFilmInfoTableCompanion(
      kinopoiskId: Value(kinopoiskId),
      imdbId:
          imdbId == null && nullToAbsent ? const Value.absent() : Value(imdbId),
      nameRu:
          nameRu == null && nullToAbsent ? const Value.absent() : Value(nameRu),
      nameEn:
          nameEn == null && nullToAbsent ? const Value.absent() : Value(nameEn),
      nameOriginal: nameOriginal == null && nullToAbsent
          ? const Value.absent()
          : Value(nameOriginal),
      posterUrl: posterUrl == null && nullToAbsent
          ? const Value.absent()
          : Value(posterUrl),
      posterUrlPreview: posterUrlPreview == null && nullToAbsent
          ? const Value.absent()
          : Value(posterUrlPreview),
      ratingKinopoisk: ratingKinopoisk == null && nullToAbsent
          ? const Value.absent()
          : Value(ratingKinopoisk),
      ratingImdb: ratingImdb == null && nullToAbsent
          ? const Value.absent()
          : Value(ratingImdb),
      year: year == null && nullToAbsent ? const Value.absent() : Value(year),
      description: description == null && nullToAbsent
          ? const Value.absent()
          : Value(description),
      shortDescription: shortDescription == null && nullToAbsent
          ? const Value.absent()
          : Value(shortDescription),
      countries: countries == null && nullToAbsent
          ? const Value.absent()
          : Value(countries),
      genres:
          genres == null && nullToAbsent ? const Value.absent() : Value(genres),
    );
  }

  factory FilmDbEntity.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FilmDbEntity(
      kinopoiskId: serializer.fromJson<int>(json['kinopoiskId']),
      imdbId: serializer.fromJson<String?>(json['imdbId']),
      nameRu: serializer.fromJson<String?>(json['nameRu']),
      nameEn: serializer.fromJson<String?>(json['nameEn']),
      nameOriginal: serializer.fromJson<String?>(json['nameOriginal']),
      posterUrl: serializer.fromJson<String?>(json['posterUrl']),
      posterUrlPreview: serializer.fromJson<String?>(json['posterUrlPreview']),
      ratingKinopoisk: serializer.fromJson<double?>(json['ratingKinopoisk']),
      ratingImdb: serializer.fromJson<double?>(json['ratingImdb']),
      year: serializer.fromJson<int?>(json['year']),
      description: serializer.fromJson<String?>(json['description']),
      shortDescription: serializer.fromJson<String?>(json['shortDescription']),
      countries: serializer.fromJson<String?>(json['countries']),
      genres: serializer.fromJson<String?>(json['genres']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'kinopoiskId': serializer.toJson<int>(kinopoiskId),
      'imdbId': serializer.toJson<String?>(imdbId),
      'nameRu': serializer.toJson<String?>(nameRu),
      'nameEn': serializer.toJson<String?>(nameEn),
      'nameOriginal': serializer.toJson<String?>(nameOriginal),
      'posterUrl': serializer.toJson<String?>(posterUrl),
      'posterUrlPreview': serializer.toJson<String?>(posterUrlPreview),
      'ratingKinopoisk': serializer.toJson<double?>(ratingKinopoisk),
      'ratingImdb': serializer.toJson<double?>(ratingImdb),
      'year': serializer.toJson<int?>(year),
      'description': serializer.toJson<String?>(description),
      'shortDescription': serializer.toJson<String?>(shortDescription),
      'countries': serializer.toJson<String?>(countries),
      'genres': serializer.toJson<String?>(genres),
    };
  }

  FilmDbEntity copyWith(
          {int? kinopoiskId,
          Value<String?> imdbId = const Value.absent(),
          Value<String?> nameRu = const Value.absent(),
          Value<String?> nameEn = const Value.absent(),
          Value<String?> nameOriginal = const Value.absent(),
          Value<String?> posterUrl = const Value.absent(),
          Value<String?> posterUrlPreview = const Value.absent(),
          Value<double?> ratingKinopoisk = const Value.absent(),
          Value<double?> ratingImdb = const Value.absent(),
          Value<int?> year = const Value.absent(),
          Value<String?> description = const Value.absent(),
          Value<String?> shortDescription = const Value.absent(),
          Value<String?> countries = const Value.absent(),
          Value<String?> genres = const Value.absent()}) =>
      FilmDbEntity(
        kinopoiskId: kinopoiskId ?? this.kinopoiskId,
        imdbId: imdbId.present ? imdbId.value : this.imdbId,
        nameRu: nameRu.present ? nameRu.value : this.nameRu,
        nameEn: nameEn.present ? nameEn.value : this.nameEn,
        nameOriginal:
            nameOriginal.present ? nameOriginal.value : this.nameOriginal,
        posterUrl: posterUrl.present ? posterUrl.value : this.posterUrl,
        posterUrlPreview: posterUrlPreview.present
            ? posterUrlPreview.value
            : this.posterUrlPreview,
        ratingKinopoisk: ratingKinopoisk.present
            ? ratingKinopoisk.value
            : this.ratingKinopoisk,
        ratingImdb: ratingImdb.present ? ratingImdb.value : this.ratingImdb,
        year: year.present ? year.value : this.year,
        description: description.present ? description.value : this.description,
        shortDescription: shortDescription.present
            ? shortDescription.value
            : this.shortDescription,
        countries: countries.present ? countries.value : this.countries,
        genres: genres.present ? genres.value : this.genres,
      );
  @override
  String toString() {
    return (StringBuffer('FilmDbEntity(')
          ..write('kinopoiskId: $kinopoiskId, ')
          ..write('imdbId: $imdbId, ')
          ..write('nameRu: $nameRu, ')
          ..write('nameEn: $nameEn, ')
          ..write('nameOriginal: $nameOriginal, ')
          ..write('posterUrl: $posterUrl, ')
          ..write('posterUrlPreview: $posterUrlPreview, ')
          ..write('ratingKinopoisk: $ratingKinopoisk, ')
          ..write('ratingImdb: $ratingImdb, ')
          ..write('year: $year, ')
          ..write('description: $description, ')
          ..write('shortDescription: $shortDescription, ')
          ..write('countries: $countries, ')
          ..write('genres: $genres')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      kinopoiskId,
      imdbId,
      nameRu,
      nameEn,
      nameOriginal,
      posterUrl,
      posterUrlPreview,
      ratingKinopoisk,
      ratingImdb,
      year,
      description,
      shortDescription,
      countries,
      genres);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FilmDbEntity &&
          other.kinopoiskId == this.kinopoiskId &&
          other.imdbId == this.imdbId &&
          other.nameRu == this.nameRu &&
          other.nameEn == this.nameEn &&
          other.nameOriginal == this.nameOriginal &&
          other.posterUrl == this.posterUrl &&
          other.posterUrlPreview == this.posterUrlPreview &&
          other.ratingKinopoisk == this.ratingKinopoisk &&
          other.ratingImdb == this.ratingImdb &&
          other.year == this.year &&
          other.description == this.description &&
          other.shortDescription == this.shortDescription &&
          other.countries == this.countries &&
          other.genres == this.genres);
}

class FullFilmInfoTableCompanion extends UpdateCompanion<FilmDbEntity> {
  final Value<int> kinopoiskId;
  final Value<String?> imdbId;
  final Value<String?> nameRu;
  final Value<String?> nameEn;
  final Value<String?> nameOriginal;
  final Value<String?> posterUrl;
  final Value<String?> posterUrlPreview;
  final Value<double?> ratingKinopoisk;
  final Value<double?> ratingImdb;
  final Value<int?> year;
  final Value<String?> description;
  final Value<String?> shortDescription;
  final Value<String?> countries;
  final Value<String?> genres;
  const FullFilmInfoTableCompanion({
    this.kinopoiskId = const Value.absent(),
    this.imdbId = const Value.absent(),
    this.nameRu = const Value.absent(),
    this.nameEn = const Value.absent(),
    this.nameOriginal = const Value.absent(),
    this.posterUrl = const Value.absent(),
    this.posterUrlPreview = const Value.absent(),
    this.ratingKinopoisk = const Value.absent(),
    this.ratingImdb = const Value.absent(),
    this.year = const Value.absent(),
    this.description = const Value.absent(),
    this.shortDescription = const Value.absent(),
    this.countries = const Value.absent(),
    this.genres = const Value.absent(),
  });
  FullFilmInfoTableCompanion.insert({
    this.kinopoiskId = const Value.absent(),
    this.imdbId = const Value.absent(),
    this.nameRu = const Value.absent(),
    this.nameEn = const Value.absent(),
    this.nameOriginal = const Value.absent(),
    this.posterUrl = const Value.absent(),
    this.posterUrlPreview = const Value.absent(),
    this.ratingKinopoisk = const Value.absent(),
    this.ratingImdb = const Value.absent(),
    this.year = const Value.absent(),
    this.description = const Value.absent(),
    this.shortDescription = const Value.absent(),
    this.countries = const Value.absent(),
    this.genres = const Value.absent(),
  });
  static Insertable<FilmDbEntity> custom({
    Expression<int>? kinopoiskId,
    Expression<String>? imdbId,
    Expression<String>? nameRu,
    Expression<String>? nameEn,
    Expression<String>? nameOriginal,
    Expression<String>? posterUrl,
    Expression<String>? posterUrlPreview,
    Expression<double>? ratingKinopoisk,
    Expression<double>? ratingImdb,
    Expression<int>? year,
    Expression<String>? description,
    Expression<String>? shortDescription,
    Expression<String>? countries,
    Expression<String>? genres,
  }) {
    return RawValuesInsertable({
      if (kinopoiskId != null) 'kinopoisk_id': kinopoiskId,
      if (imdbId != null) 'imdb_id': imdbId,
      if (nameRu != null) 'name_ru': nameRu,
      if (nameEn != null) 'name_en': nameEn,
      if (nameOriginal != null) 'name_original': nameOriginal,
      if (posterUrl != null) 'poster_url': posterUrl,
      if (posterUrlPreview != null) 'poster_url_preview': posterUrlPreview,
      if (ratingKinopoisk != null) 'rating_kinopoisk': ratingKinopoisk,
      if (ratingImdb != null) 'rating_imdb': ratingImdb,
      if (year != null) 'year': year,
      if (description != null) 'description': description,
      if (shortDescription != null) 'short_description': shortDescription,
      if (countries != null) 'countries': countries,
      if (genres != null) 'genres': genres,
    });
  }

  FullFilmInfoTableCompanion copyWith(
      {Value<int>? kinopoiskId,
      Value<String?>? imdbId,
      Value<String?>? nameRu,
      Value<String?>? nameEn,
      Value<String?>? nameOriginal,
      Value<String?>? posterUrl,
      Value<String?>? posterUrlPreview,
      Value<double?>? ratingKinopoisk,
      Value<double?>? ratingImdb,
      Value<int?>? year,
      Value<String?>? description,
      Value<String?>? shortDescription,
      Value<String?>? countries,
      Value<String?>? genres}) {
    return FullFilmInfoTableCompanion(
      kinopoiskId: kinopoiskId ?? this.kinopoiskId,
      imdbId: imdbId ?? this.imdbId,
      nameRu: nameRu ?? this.nameRu,
      nameEn: nameEn ?? this.nameEn,
      nameOriginal: nameOriginal ?? this.nameOriginal,
      posterUrl: posterUrl ?? this.posterUrl,
      posterUrlPreview: posterUrlPreview ?? this.posterUrlPreview,
      ratingKinopoisk: ratingKinopoisk ?? this.ratingKinopoisk,
      ratingImdb: ratingImdb ?? this.ratingImdb,
      year: year ?? this.year,
      description: description ?? this.description,
      shortDescription: shortDescription ?? this.shortDescription,
      countries: countries ?? this.countries,
      genres: genres ?? this.genres,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (kinopoiskId.present) {
      map['kinopoisk_id'] = Variable<int>(kinopoiskId.value);
    }
    if (imdbId.present) {
      map['imdb_id'] = Variable<String>(imdbId.value);
    }
    if (nameRu.present) {
      map['name_ru'] = Variable<String>(nameRu.value);
    }
    if (nameEn.present) {
      map['name_en'] = Variable<String>(nameEn.value);
    }
    if (nameOriginal.present) {
      map['name_original'] = Variable<String>(nameOriginal.value);
    }
    if (posterUrl.present) {
      map['poster_url'] = Variable<String>(posterUrl.value);
    }
    if (posterUrlPreview.present) {
      map['poster_url_preview'] = Variable<String>(posterUrlPreview.value);
    }
    if (ratingKinopoisk.present) {
      map['rating_kinopoisk'] = Variable<double>(ratingKinopoisk.value);
    }
    if (ratingImdb.present) {
      map['rating_imdb'] = Variable<double>(ratingImdb.value);
    }
    if (year.present) {
      map['year'] = Variable<int>(year.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (shortDescription.present) {
      map['short_description'] = Variable<String>(shortDescription.value);
    }
    if (countries.present) {
      map['countries'] = Variable<String>(countries.value);
    }
    if (genres.present) {
      map['genres'] = Variable<String>(genres.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FullFilmInfoTableCompanion(')
          ..write('kinopoiskId: $kinopoiskId, ')
          ..write('imdbId: $imdbId, ')
          ..write('nameRu: $nameRu, ')
          ..write('nameEn: $nameEn, ')
          ..write('nameOriginal: $nameOriginal, ')
          ..write('posterUrl: $posterUrl, ')
          ..write('posterUrlPreview: $posterUrlPreview, ')
          ..write('ratingKinopoisk: $ratingKinopoisk, ')
          ..write('ratingImdb: $ratingImdb, ')
          ..write('year: $year, ')
          ..write('description: $description, ')
          ..write('shortDescription: $shortDescription, ')
          ..write('countries: $countries, ')
          ..write('genres: $genres')
          ..write(')'))
        .toString();
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(e);
  late final $FilmMinimizedTableTable filmMinimizedTable =
      $FilmMinimizedTableTable(this);
  late final $FullFilmInfoTableTable fullFilmInfoTable =
      $FullFilmInfoTableTable(this);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [filmMinimizedTable, fullFilmInfoTable];
}
