import 'dart:convert';

import 'package:zamyatin_ay/api/types/country_entity.dart';
import 'package:zamyatin_ay/api/types/film_entity.dart';
import 'package:zamyatin_ay/api/types/genre_entity.dart';
import 'package:zamyatin_ay/database/app_database.dart';

class FilmMapper {
  FilmMapper._();

  static FilmEntity databaseToEntity(FilmDbEntity filmDb) {
    return FilmEntity(
      filmDb.kinopoiskId,
      filmDb.imdbId,
      filmDb.nameRu,
      filmDb.nameEn,
      filmDb.nameOriginal,
      filmDb.posterUrl,
      filmDb.posterUrlPreview,
      filmDb.ratingKinopoisk,
      filmDb.ratingImdb,
      filmDb.year,
      filmDb.description,
      filmDb.shortDescription,
      filmDb.countries == null
          ? null
          : (jsonDecode(filmDb.countries!) as List<dynamic>)
              .map((e) => CountryEntity.fromJson(e))
              .toList(),
      filmDb.genres == null
          ? null
          : (jsonDecode(filmDb.genres!) as List<dynamic>)
              .map((e) => GenreEntity.fromJson(e))
              .toList(),
    );
  }

  static List<FilmEntity> databaseListToEntityList(
      List<FilmDbEntity> filmsDbList) {
    List<FilmEntity> films = [];
    final length = filmsDbList.length;
    for (int i = 0; i < length; i++) {
      films.add(databaseToEntity(filmsDbList[i]));
    }

    return films;
  }

  static FilmDbEntity entityToDatabase(FilmEntity film) {
    return FilmDbEntity(
        kinopoiskId: film.kinopoiskId,
        imdbId: film.imdbId,
        nameRu: film.nameRu,
        nameEn: film.nameEn,
        nameOriginal: film.nameOriginal,
        posterUrl: film.posterUrl,
        posterUrlPreview: film.posterUrlPreview,
        ratingKinopoisk: film.ratingKinopoisk?.toDouble(),
        ratingImdb: film.ratingImdb?.toDouble(),
        year: film.year,
        description: film.description,
        shortDescription: film.shortDescription,
        countries: jsonEncode(film.countries?.map((e) => e.toJson()).toList()),
        genres: jsonEncode(film.genres?.map((e) => e.toJson()).toList()));
  }

  static List<FilmDbEntity> entityListToDatabaseList(
      List<FilmEntity> filmsList) {
    List<FilmDbEntity> dbFilmsList = [];
    final length = filmsList.length;
    for (int i = 0; i < length; i++) {
      dbFilmsList.add(entityToDatabase(filmsList[i]));
    }

    return dbFilmsList;
  }
}
