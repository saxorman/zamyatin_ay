import 'dart:convert';

import 'package:zamyatin_ay/api/types/country_entity.dart';
import 'package:zamyatin_ay/api/types/film_minimized_entity.dart';
import 'package:zamyatin_ay/api/types/genre_entity.dart';
import 'package:zamyatin_ay/database/app_database.dart';

class FilmMinimizedMapper {
  FilmMinimizedMapper._();

  static FilmMinimizedEntity databaseToEntity(FilmDbMinimized filmDb) {
    return FilmMinimizedEntity(
      filmDb.filmId,
      filmDb.rating,
      filmDb.nameRu,
      filmDb.nameEn,
      filmDb.type,
      filmDb.posterUrl,
      filmDb.posterUrlPreview,
      filmDb.filmLength,
      filmDb.ratingVoteCount,
      filmDb.year,
      filmDb.description,
      filmDb.countries == null
          ? null
          : (jsonDecode(filmDb.countries!) as List<dynamic>)
              .map((e) => CountryEntity.fromJson(e))
              .toList(),
      filmDb.genres == null
          ? null
          : (jsonDecode(filmDb.genres!) as List<dynamic>)
              .map((e) => GenreEntity.fromJson(e))
              .toList(),
    );
  }

  static List<FilmMinimizedEntity> databaseListToEntityList(
      List<FilmDbMinimized> filmsDbList) {
    List<FilmMinimizedEntity> films = [];
    final length = filmsDbList.length;
    for (int i = 0; i < length; i++) {
      films.add(databaseToEntity(filmsDbList[i]));
    }

    return films;
  }

  static FilmDbMinimized entityToDatabase(FilmMinimizedEntity film) {
    return FilmDbMinimized(
        filmId: film.filmId,
        rating: film.rating,
        nameRu: film.nameRu,
        nameEn: film.nameEn,
        type: film.type,
        posterUrl: film.posterUrl,
        posterUrlPreview: film.posterUrlPreview,
        filmLength: film.filmLength,
        ratingVoteCount: film.ratingVoteCount,
        year: film.year,
        description: film.description,
        countries: jsonEncode(film.countries?.map((e) => e.toJson()).toList()),
        genres: jsonEncode(film.genres?.map((e) => e.toJson()).toList()));
  }

  static List<FilmDbMinimized> entityListToDatabaseList(
      List<FilmMinimizedEntity> filmsList) {
    List<FilmDbMinimized> dbFilmsList = [];
    final length = filmsList.length;
    for (int i = 0; i < length; i++) {
      dbFilmsList.add(entityToDatabase(filmsList[i]));
    }

    return dbFilmsList;
  }
}
