import 'package:drift/drift.dart';

@DataClassName("FilmDbMinimized")
class FilmMinimizedTable extends Table {
  @override
  String get tableName => "film_minimized";

  IntColumn get filmId => integer()();

  TextColumn get rating => text().nullable()();

  TextColumn get nameRu => text().nullable()();

  TextColumn get nameEn => text().nullable()();

  TextColumn get type => text().nullable()();

  TextColumn get posterUrl => text().nullable()();

  TextColumn get posterUrlPreview => text().nullable()();

  TextColumn get filmLength => text().nullable()();

  IntColumn get ratingVoteCount => integer().nullable()();

  TextColumn get year => text().nullable()();

  TextColumn get description => text().nullable()();

  TextColumn get countries => text().nullable()();

  TextColumn get genres => text().nullable()();

  @override
  Set<Column> get primaryKey => {filmId};
}
