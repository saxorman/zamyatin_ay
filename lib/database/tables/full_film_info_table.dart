import 'package:drift/drift.dart';

@DataClassName("FilmDbEntity")
class FullFilmInfoTable extends Table {
  @override
  String get tableName => "films_table";

  IntColumn get kinopoiskId => integer()();

  TextColumn get imdbId => text().nullable()();

  TextColumn get nameRu => text().nullable()();

  TextColumn get nameEn => text().nullable()();

  TextColumn get nameOriginal => text().nullable()();

  TextColumn get posterUrl => text().nullable()();

  TextColumn get posterUrlPreview => text().nullable()();

  RealColumn get ratingKinopoisk => real().nullable()();

  RealColumn get ratingImdb => real().nullable()();

  IntColumn get year => integer().nullable()();

  TextColumn get description => text().nullable()();

  TextColumn get shortDescription => text().nullable()();

  TextColumn get countries => text().nullable()();

  TextColumn get genres => text().nullable()();

  @override
  Set<Column> get primaryKey => {kinopoiskId};
}
