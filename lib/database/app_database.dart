
import 'dart:async';
import 'dart:io';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:get_it/get_it.dart';

import 'source/films_database_source.dart';
import 'tables/film_minimized_table.dart';
import 'tables/full_film_info_table.dart';

part 'app_database.g.dart';

@DriftDatabase(tables: [
  FilmMinimizedTable,
  FullFilmInfoTable
])
class AppDatabase extends _$AppDatabase implements Disposable {
  static const String _USER_DATABASE_FOLDER = "user_databases";
  AppDatabase(String userId)
      : super(_openConnection(_generateDbName(userId))) {
    filmsDatabaseSource = FilmsDatabaseSource(this);
  }

  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration {
    return MigrationStrategy(
      beforeOpen: (details) async {
        await customStatement('PRAGMA foreign_keys = ON');
      },
    );
  }

  static String _generateDbName(String userId) {
    return "$userId.db";
  }

  late final FilmsDatabaseSource filmsDatabaseSource;

  @override
  FutureOr onDispose() => close();
}

LazyDatabase _openConnection(String dbName) {
  return LazyDatabase(() async {
    final folder = Directory(join(
        (await getApplicationDocumentsDirectory()).path,
        AppDatabase._USER_DATABASE_FOLDER));
    final file = File(join(folder.path, dbName));
    return NativeDatabase.createInBackground(file);
  });
}
