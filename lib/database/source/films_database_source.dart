import 'package:zamyatin_ay/api/types/film_entity.dart';
import 'package:zamyatin_ay/api/types/film_minimized_entity.dart';
import 'package:zamyatin_ay/database/app_database.dart';
import 'package:zamyatin_ay/database/mappers/film_minimized_mapper.dart';
import 'package:zamyatin_ay/database/mappers/full_film_mapper.dart';

class FilmsDatabaseSource {
  final AppDatabase _db;

  FilmsDatabaseSource(this._db);

  Future<bool> deleteMinimized(int filmId) async {
    try {
      final query = _db.delete(_db.filmMinimizedTable)
        ..where((tbl) => tbl.filmId.equals(filmId));
      await query.go();
    } catch (e) {
      print("Error deleting film from database: $e");
      return false;
    }
    return true;
  }

  Future<bool> deleteFullFillInfo(int filmId) async {
    try {
      final query = _db.delete(_db.fullFilmInfoTable)
        ..where((tbl) => tbl.kinopoiskId.equals(filmId));
      await query.go();
    } catch (e) {
      print("Error deleting full film info from database: $e");
      return false;
    }
    return true;
  }

  Future<List<FilmMinimizedEntity>> getAllFilmsMinimized() async {
    return FilmMinimizedMapper.databaseListToEntityList(
        await _db.select(_db.filmMinimizedTable).get());
  }

  Future<List<FilmEntity>> getAllFullFilmInfo() async {
    return FilmMapper.databaseListToEntityList(
        await _db.select(_db.fullFilmInfoTable).get());
  }

  Future<FilmEntity?> getFullFilmInfoById(int filmId) async {
    final query = _db.select(_db.fullFilmInfoTable)
      ..where((tbl) => tbl.kinopoiskId.equals(filmId));

    final data = await query.getSingleOrNull();

    return data == null ? null : FilmMapper.databaseToEntity(data);
  }

  Future<bool> insertFilmMinimized(FilmMinimizedEntity film) async {
    try {
      return (await _db.into(_db.filmMinimizedTable).insertOnConflictUpdate(
          FilmMinimizedMapper.entityToDatabase(film))) > 0;
    } catch (e) {
      return false;
    }
  }

  Future<bool> insertFilmFullInfo(FilmEntity film) async {
    try {
      return (await _db.into(_db.fullFilmInfoTable).insertOnConflictUpdate(
          FilmMapper.entityToDatabase(film))) > 0;
    } catch (e) {
      return false;
    }
  }

  Future<bool> insertListFilmMinimized(List<FilmMinimizedEntity> filmsList) async {
    try {
      await _db.batch((batch) {
        batch.insertAllOnConflictUpdate(_db.filmMinimizedTable,
            FilmMinimizedMapper.entityListToDatabaseList(filmsList));
      });
    } catch (e) {
      return false;
    }

    return true;
  }

  Future deleteAllMinimized() async {
    await _db.batch((batch) => batch.deleteAll(_db.filmMinimizedTable));
  }

  Future deleteAllFilmsFullInfo() async {
    await _db.batch((batch) => batch.deleteAll(_db.fullFilmInfoTable));
  }
}