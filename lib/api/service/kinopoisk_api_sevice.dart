import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:zamyatin_ay/api/types/film_entity.dart';
import 'package:zamyatin_ay/api/types/response/search_films_response.dart';
import 'package:zamyatin_ay/api/types/response/top_films_response.dart';

part 'kinopoisk_api_sevice.g.dart';

@RestApi()
abstract class KinopoiskApiService /* implements ApiServiceInterface*/ {
  factory KinopoiskApiService(Dio dio, {String baseUrl}) = _KinopoiskApiService;

  @GET("/api/v2.2/films/top")
  Future<TopFilmsResponse> getTopFilms(
      {@Query("type") String type = "TOP_100_POPULAR_FILMS"});

  @GET("/api/v2.2/films")
  Future<SearchFilmsResponse> searchFilms(@Query("keyword") String keyword,
      {@Query("type") String type = "FILM",
      @Query("order") String order = "RATING",
      @Query("ratingFrom") int ratingFrom = 0,
      @Query("ratingTo") int ratingTo = 10,
      @Query("yearFrom") int yearFrom = 1000,
      @Query("yearTo") int yearTo = 3000,
      @Query("page") int page = 1,
      });

  @GET("/api/v2.2/films/{id}")
  Future<FilmEntity> getFilmById(@Path() int id);

}
