import 'package:dio/dio.dart';
import 'package:zamyatin_ay/api/service/kinopoisk_api_sevice.dart';

class KinopoiskApi {
  late final KinopoiskApiService api;
  late Dio _dioClient;

  KinopoiskApi() {
    var options = BaseOptions()
      ..baseUrl = "https://kinopoiskapiunofficial.tech"
      ..receiveTimeout = const Duration(seconds: 60)
      ..connectTimeout = const Duration(seconds: 10)
      ..headers["x-api-key"] = "e30ffed0-76ab-4dd6-b41f-4c9da2b2735b";

    _dioClient = Dio(options);

    api = KinopoiskApiService(_dioClient);
  }
}
