import 'package:json_annotation/json_annotation.dart';
import 'package:zamyatin_ay/api/types/film_entity.dart';

part 'search_films_response.g.dart';

@JsonSerializable(explicitToJson: false, createToJson: false)
class SearchFilmsResponse {
  SearchFilmsResponse(this.items, this.total, this.totalPages);

  final int? total;
  final int? totalPages;
  final List<FilmEntity> items;

  factory SearchFilmsResponse.fromJson(Map<String, dynamic> json) =>
      _$SearchFilmsResponseFromJson(json);
}