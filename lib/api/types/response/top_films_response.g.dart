// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'top_films_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TopFilmsResponse _$TopFilmsResponseFromJson(Map<String, dynamic> json) =>
    TopFilmsResponse(
      json['pagesCount'] as int,
      (json['films'] as List<dynamic>)
          .map((e) => FilmMinimizedEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['keyword'] as String?,
      json['searchFilmsCountResult'] as int?,
    );
