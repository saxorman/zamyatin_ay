// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_films_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchFilmsResponse _$SearchFilmsResponseFromJson(Map<String, dynamic> json) =>
    SearchFilmsResponse(
      (json['items'] as List<dynamic>)
          .map((e) => FilmEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['total'] as int?,
      json['totalPages'] as int?,
    );
