import 'package:json_annotation/json_annotation.dart';
import 'package:zamyatin_ay/api/types/film_minimized_entity.dart';

part 'top_films_response.g.dart';

@JsonSerializable(explicitToJson: false, createToJson: false)
class TopFilmsResponse {
  TopFilmsResponse(
      this.pagesCount, this.films, this.keyword, this.searchFilmsCountResult);

  final String? keyword;
  final int pagesCount;
  final int? searchFilmsCountResult;
  final List<FilmMinimizedEntity> films;

  factory TopFilmsResponse.fromJson(Map<String, dynamic> json) =>
      _$TopFilmsResponseFromJson(json);
}
