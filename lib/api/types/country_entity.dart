import 'package:json_annotation/json_annotation.dart';

part 'country_entity.g.dart';

@JsonSerializable(explicitToJson: true)
class CountryEntity {
  CountryEntity(this.country);

  final String country;

  factory CountryEntity.fromJson(Map<String, dynamic> json) =>
      _$CountryEntityFromJson(json);

  Map<String, dynamic> toJson() => _$CountryEntityToJson(this);
}
