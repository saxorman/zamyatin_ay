// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'film_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FilmEntity _$FilmEntityFromJson(Map<String, dynamic> json) => FilmEntity(
      json['kinopoiskId'] as int,
      json['imdbId'] as String?,
      json['nameRu'] as String?,
      json['nameEn'] as String?,
      json['nameOriginal'] as String?,
      json['posterUrl'] as String?,
      json['posterUrlPreview'] as String?,
      json['ratingKinopoisk'] as num?,
      json['ratingImdb'] as num?,
      json['year'] as int?,
      json['description'] as String?,
      json['shortDescription'] as String?,
      (json['countries'] as List<dynamic>?)
          ?.map((e) => CountryEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['genres'] as List<dynamic>?)
          ?.map((e) => GenreEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$FilmEntityToJson(FilmEntity instance) =>
    <String, dynamic>{
      'kinopoiskId': instance.kinopoiskId,
      'imdbId': instance.imdbId,
      'nameRu': instance.nameRu,
      'nameEn': instance.nameEn,
      'nameOriginal': instance.nameOriginal,
      'posterUrl': instance.posterUrl,
      'posterUrlPreview': instance.posterUrlPreview,
      'ratingKinopoisk': instance.ratingKinopoisk,
      'ratingImdb': instance.ratingImdb,
      'year': instance.year,
      'description': instance.description,
      'shortDescription': instance.shortDescription,
      'countries': instance.countries?.map((e) => e.toJson()).toList(),
      'genres': instance.genres?.map((e) => e.toJson()).toList(),
    };
