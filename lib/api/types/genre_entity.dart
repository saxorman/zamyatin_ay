import 'package:json_annotation/json_annotation.dart';

part 'genre_entity.g.dart';

@JsonSerializable(explicitToJson: true)
class GenreEntity {
  GenreEntity(this.genre);

  final String genre;

  factory GenreEntity.fromJson(Map<String, dynamic> json) =>
      _$GenreEntityFromJson(json);

  Map<String, dynamic> toJson() => _$GenreEntityToJson(this);
}
