// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'genre_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GenreEntity _$GenreEntityFromJson(Map<String, dynamic> json) => GenreEntity(
      json['genre'] as String,
    );

Map<String, dynamic> _$GenreEntityToJson(GenreEntity instance) =>
    <String, dynamic>{
      'genre': instance.genre,
    };
