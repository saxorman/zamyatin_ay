import 'package:json_annotation/json_annotation.dart';

import 'country_entity.dart';
import 'genre_entity.dart';

part 'film_entity.g.dart';

//TODO: change later
@JsonSerializable(explicitToJson: true)
class FilmEntity {
  FilmEntity(
      this.kinopoiskId,
      this.imdbId,
      this.nameRu,
      this.nameEn,
      this.nameOriginal,
      this.posterUrl,
      this.posterUrlPreview,
      this.ratingKinopoisk,
      this.ratingImdb,
      this.year,
      this.description,
      this.shortDescription,
      this.countries,
      this.genres);

  final int kinopoiskId;
  final String? imdbId;
  final String? nameRu;
  final String? nameEn;
  final String? nameOriginal;
  final String? posterUrl;
  final String? posterUrlPreview;
  final num? ratingKinopoisk;
  final num? ratingImdb;
  final int? year;
  final String? description;
  final String? shortDescription;
  final List<CountryEntity>? countries;
  final List<GenreEntity>? genres;

  factory FilmEntity.fromJson(Map<String, dynamic> json) =>
      _$FilmEntityFromJson(json);

  Map<String, dynamic> toJson() => _$FilmEntityToJson(this);
}
