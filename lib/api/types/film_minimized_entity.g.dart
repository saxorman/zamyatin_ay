// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'film_minimized_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FilmMinimizedEntity _$FilmMinimizedEntityFromJson(Map<String, dynamic> json) =>
    FilmMinimizedEntity(
      json['filmId'] as int,
      json['rating'] as String?,
      json['nameRu'] as String?,
      json['nameEn'] as String?,
      json['type'] as String?,
      json['posterUrl'] as String?,
      json['posterUrlPreview'] as String?,
      json['filmLength'] as String?,
      json['ratingVoteCount'] as int?,
      json['year'] as String?,
      json['description'] as String?,
      (json['countries'] as List<dynamic>?)
          ?.map((e) => CountryEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['genres'] as List<dynamic>?)
          ?.map((e) => GenreEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$FilmMinimizedEntityToJson(
        FilmMinimizedEntity instance) =>
    <String, dynamic>{
      'filmId': instance.filmId,
      'rating': instance.rating,
      'nameRu': instance.nameRu,
      'nameEn': instance.nameEn,
      'type': instance.type,
      'posterUrl': instance.posterUrl,
      'posterUrlPreview': instance.posterUrlPreview,
      'filmLength': instance.filmLength,
      'ratingVoteCount': instance.ratingVoteCount,
      'year': instance.year,
      'description': instance.description,
      'countries': instance.countries?.map((e) => e.toJson()).toList(),
      'genres': instance.genres?.map((e) => e.toJson()).toList(),
    };
