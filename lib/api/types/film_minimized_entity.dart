import 'package:json_annotation/json_annotation.dart';

import 'country_entity.dart';
import 'genre_entity.dart';

part 'film_minimized_entity.g.dart';

@JsonSerializable(explicitToJson: true)
class FilmMinimizedEntity {
  FilmMinimizedEntity(
      this.filmId,
      this.rating,
      this.nameRu,
      this.nameEn,
      this.type,
      this.posterUrl,
      this.posterUrlPreview,
      this.filmLength,
      this.ratingVoteCount,
      this.year,
      this.description,
      this.countries,
      this.genres);

  final int filmId;
  final String? rating;
  final String? nameRu;
  final String? nameEn;
  final String? type;
  final String? posterUrl;
  final String? posterUrlPreview;
  final String? filmLength;
  final int? ratingVoteCount;
  final String? year;
  final String? description;
  final List<CountryEntity>? countries;
  final List<GenreEntity>? genres;
  @JsonKey(includeFromJson: false, includeToJson: false)
  bool isFavorite = false;

  factory FilmMinimizedEntity.fromJson(Map<String, dynamic> json) =>
      _$FilmMinimizedEntityFromJson(json);

  Map<String, dynamic> toJson() => _$FilmMinimizedEntityToJson(this);
}
