import 'package:get_it/get_it.dart';
import 'package:zamyatin_ay/api/kinopoisk_api.dart';
import 'package:zamyatin_ay/database/app_database.dart';
import 'package:zamyatin_ay/database/source/films_database_source.dart';
import 'package:zamyatin_ay/models/films_model.dart';
import 'package:zamyatin_ay/repository/films_repository.dart';

void setupSingletons() {
  final di = GetIt.instance;

  di.registerLazySingleton(() => AppDatabase("user"));
  di.registerLazySingleton(() => FilmsDatabaseSource(di<AppDatabase>()));
  di.registerLazySingleton(() => KinopoiskApi());
  di.registerLazySingleton(() => FilmsRepository(
      di<KinopoiskApi>(), di<AppDatabase>().filmsDatabaseSource));
  di.registerLazySingleton(() => FilmsModel(di<FilmsRepository>()));
}

Future<void> unregisterSingletons() async {
  final di = GetIt.instance;

  await Future.wait([
    di.unregister<FilmsModel>(),
    di.unregister<FilmsRepository>(),
    di.unregister<KinopoiskApi>(),
  ].map((unregFunc) => Future.value(unregFunc)));
}
